// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TPSForSkillbox : ModuleRules
{
	public TPSForSkillbox(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicIncludePaths.AddRange(new string[] { "TPS" });

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule", "PhysicsCore", "Slate" });
    }
}
