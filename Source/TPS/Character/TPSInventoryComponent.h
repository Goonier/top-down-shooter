// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "FunctionLibrary/Types.h"
#include "Game/TPSGameInstance.h"
#include "TPSInventoryComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnSwitchWeapon, FName, WeaponIdName, FAddicionalWeaponInfo, WeaponAdditionalInfo, int32, NewCurrentIndexWeapon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAmmoChange, EWeaponType, TypeAmmo, int32, Cout);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponAdditionalInfoChange, int32, IndexSlot, FAddicionalWeaponInfo, AddicionalInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponAmmoEmpty, EWeaponType, WeaponType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponAmmoAvailable, EWeaponType, WeaponType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnUpdateWeaponSlots, int32, IndexSlotChange, FWeaponSlot, NewInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponNotHaveRound, int32, IndexSlotWeapon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponHaveRound, int32, IndexSlotWeapon);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPSFORSKILLBOX_API UTPSInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTPSInventoryComponent();

UPROPERTY(BlueprintAssignable, Category = "Inventory")
FOnSwitchWeapon OnSwitchWeapon;
UPROPERTY(BlueprintAssignable, Category = "Inventory")
FOnAmmoChange OnAmmoChange;
UPROPERTY(BlueprintAssignable, Category = "Inventory")
FOnWeaponAdditionalInfoChange OnWeaponAdditionalInfoChange;
UPROPERTY(BlueprintAssignable, Category = "Inventory")
FOnWeaponAmmoEmpty OnWeaponAmmoEmpty;
UPROPERTY(BlueprintAssignable, Category = "Inventory")
FOnWeaponAmmoAvailable OnWeaponAmmoAvailable;
UPROPERTY(BlueprintAssignable, Category = "Inventory")
FOnUpdateWeaponSlots OnUpdateWeaponSlots;

UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
FOnWeaponNotHaveRound OnWeaponNotHaveRound;
UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
FOnWeaponHaveRound OnWeaponHaveRound;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	bool SwitchWeaponByIndex(int32 IndexWeaponToChange, int32 PreviosIndex, FAddicionalWeaponInfo PreviosWeaponInfo);

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapons")
		TArray<FWeaponSlot> WeaponSlots;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapons")
		TArray<FAmmoSlot> AmmoSlots;

	int32 MaxSlotsWeapon = 0;

	bool SwitchWeaponToIndexByNextPreviosIndex(int32 ChangeToIndex, int32 OldIndex, FAddicionalWeaponInfo OldInfo, bool bIsForward);

	FAddicionalWeaponInfo GetAdditionInfoWeapon(int32 IndexWeapon);
	int32 GetWeaponIndexSlotByName(FName IdWeaponName);
	FName GetWeaponNameBySlotIndex(int32 IndexSlot);
	void SetAdditionalInfoWeapon(int32 IndexWeapon, FAddicionalWeaponInfo NewInfo);

	bool GetWeaponTypeByNameWeapon(FName IdWeaponName, EWeaponType &WeaponType);

	UFUNCTION(BlueprintCallable)
	void AmmoSlotChangeValue(EWeaponType TypeWeapon, int32 CoutChangeAmmo);
	bool CheckAmmoForWeapon(EWeaponType TypeWeapon, int8 &AvailableAmmoForWeapon);

	//Interface PickUp Actors
	UFUNCTION(BlueprintCallable, Category = "Interface")
	bool CheckCanTakeAmmo(EWeaponType AmmoType);
	UFUNCTION(BlueprintCallable, Category = "Interface")
	bool CheckCanTakeWeapon(int32 &FreeSlot);
	UFUNCTION(BlueprintCallable, Category = "Interface")
	bool SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 IndexSlot, int32 CurrentIndexWeaponChar, FDropItem& DropItemInfo);
	UFUNCTION(BlueprintCallable, Category = "Interface")
	bool GetDropItemInfoFromInventory(int32 IndexSlot, FDropItem &DropItemInfo);
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Interface")
	void TryGetWeaponToInventory_OnServer(AActor* PickUpActor, FWeaponSlot NewWeapon);

	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Interface")
	void DropWeapobByIndex_OnServer(int32 ByIndex);

	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Inv")
	void InitInventory_OnServer(const TArray<FWeaponSlot>& NewWeaponSlotsInfo, const TArray<FAmmoSlot>& NewAmmoSlotsInfo);

	UFUNCTION(Server, Reliable)
	void SwitchWeaponEvent_OnServer(FName WeaponIdName, FAddicionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);
	UFUNCTION(NetMulticast, Reliable)
	void AmmoChangeEvent_Multicast(EWeaponType TypeWeapon, int32 Cout);
	UFUNCTION(NetMulticast, Reliable)
	void WeaponAdditionalInfoChangeEvent_Multicast(int32 IndexSlot, FAddicionalWeaponInfo AddicionalInfo);
	UFUNCTION(NetMulticast, Reliable)
	void WeaponAmmoEmptyEvent_Multicast(EWeaponType WeaponType);
	UFUNCTION(NetMulticast, Reliable)
	void WeaponAmmoAvailableEvent_Multicast(EWeaponType WeaponType);
	UFUNCTION(NetMulticast, Reliable)
	void UpdateWeaponSlotsEvent_Multicast(int32 IndexSlotChange, FWeaponSlot NewInfo);

};
