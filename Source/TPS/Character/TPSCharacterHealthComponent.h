// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Character/TPSHealthComponent.h"
#include "TPSCharacterHealthComponent.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnShieldBroke);

UCLASS()
class TPSFORSKILLBOX_API UTPSCharacterHealthComponent : public UTPSHealthComponent
{
	GENERATED_BODY()

protected:

	float Shield = 100.0f;
	
public:
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Shield")
	FOnShieldChange OnShieldChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Shield")
	FOnShieldBroke OnShieldBroke;

	FTimerHandle TimerHandle_CollDownShieldTimer;
	FTimerHandle TimerHandle_ShieldRecoveryRateTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	float CoolDownShieldRecoveryTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	float ShieldRecoveryValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	float ShieldRecoveryRate = 0.1f;

	void ChangeHealthValue_OnServer(float ChangeValue) override;

	float GetCurrentShield();

	void ChangeShieldValue(float ChangeValue);

	void CoolDownShieldEnd();

	void RecoveryShield();

	UFUNCTION(BlueprintCallable)
	float GetShieldValue();

	UFUNCTION(NetMulticast, Reliable)
	void ShieldChangeEvent_Multicast(float ShieldHave, float Damage);
	UFUNCTION(NetMulticast, Reliable)
	void ShieldBrokeEvent_Multicast();
};
