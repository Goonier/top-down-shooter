// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TPSForSkillboxGameMode.generated.h"

UCLASS(minimalapi)
class ATPSForSkillboxGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATPSForSkillboxGameMode();

	void PlayerCharacterDead();
};



