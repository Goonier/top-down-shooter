// Copyright Epic Games, Inc. All Rights Reserved.

#include "TPSForSkillbox.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TPSForSkillbox, "TPSForSkillbox" );

DEFINE_LOG_CATEGORY(LogTPSForSkillbox)
DEFINE_LOG_CATEGORY(LogTPSForSkillbox_Net)
 